﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public GameObject leftBorder;
    public GameObject rightBorder;
    public Rigidbody2D rigidBody;
    public GroundDetection groundDetection;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private CollisionDamage collisionDamage;


    public bool isRightDirection;
    

    public float speed;

      private void Update()
    {
        if (groundDetection.isGrounded)
        {
            if (transform.position.x > rightBorder.transform.position.x
                || collisionDamage.Direction < 0 )
                isRightDirection = false;
            else if (transform.position.x < leftBorder.transform.position.x
                || collisionDamage.Direction > 0 )
                isRightDirection = true;
            rigidBody.velocity = isRightDirection ? Vector2.right : Vector2.left;
            rigidBody.velocity *= speed;
        }

        if (rigidBody.velocity.x > 0)
            spriteRenderer.flipX = true;
        if (rigidBody.velocity.x < 0)
            spriteRenderer.flipX = false;
    }
}


