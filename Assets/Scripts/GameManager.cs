﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager Instance { get; private set; }
    #endregion

    [SerializeField] private GameObject inventoryPanel;
    [SerializeField] private GameObject pauseMenu;
    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject, Coin> coinContainer;
    public Dictionary<GameObject, BuffReciever> buffRecieverContainer;
    public Dictionary<GameObject, ItemComponent> itemsContainer;
    [HideInInspector] public PlayerInventory inventory;
    public ItemBase itemDataBase;
    private bool isInventoryOn;



    private void Awake()
    {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();
        coinContainer = new Dictionary<GameObject, Coin>();
        buffRecieverContainer = new Dictionary<GameObject, BuffReciever>();
        itemsContainer = new Dictionary<GameObject, ItemComponent>();
    }

    public void OnClickPause()
    {
        if (Time.timeScale > 0)
        {
            pauseMenu.gameObject.SetActive(true);
            Time.timeScale = 0;
        }

        else
        {
            pauseMenu.gameObject.SetActive(false);
            Time.timeScale = 1;
        }

    }

    public void OnCliclInventory()
    {
        if (isInventoryOn == true)
        {
            inventoryPanel.gameObject.SetActive(false);
            isInventoryOn = false;
        }

        else
        {
            inventoryPanel.gameObject.SetActive(true);
            isInventoryOn = true;
        }

    }
}
