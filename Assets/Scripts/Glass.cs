﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glass : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private Animator animator;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Health health = col.gameObject.GetComponent<Health>();
            col.gameObject.GetComponent<Animator>().SetTrigger("TakeDamage");
            health.TakeHit(damage);
            Destroy(gameObject);
        }
    }
}
