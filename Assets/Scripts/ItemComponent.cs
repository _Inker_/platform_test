﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour, IObjectDestroyer
{
    [SerializeField] private ItemType type;
    [SerializeField] private SpriteRenderer spriteRendere;
    private Item item;

    public Item Item
    {
        get { return item; }
    }

    public void Destroy(GameObject gameObject)
    {
        MonoBehaviour.Destroy(gameObject);
    }

    void Start()
    {
        item = GameManager.Instance.itemDataBase.GetItemOfID((int)type);
        spriteRendere.sprite = item.Icon;
        GameManager.Instance.itemsContainer.Add(gameObject, this);
    }

}

public enum ItemType
{
    ForcePotion = 0,
    DamagePotion = 1,    
    ArmorPotion = 2
}
