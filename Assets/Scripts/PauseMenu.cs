﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Sprite isSoundOn;
    [SerializeField] private Sprite isSoundOff;
    [SerializeField] private Image sound;

    private bool setSound;
    private int soundS;
    void Start()
    {
        if (PlayerPrefs.HasKey("Set_Sound"))
            soundS = PlayerPrefs.GetInt("Set_Sound");
        if (soundS == 1)
            sound.sprite = isSoundOn;
        else sound.sprite = isSoundOff;
    }


    public void OnClickMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClickExit()
    {
        Application.Quit();
    }

    public void OnClickSound()
    {

        if (setSound == true)
        {
            soundS = 0;
            sound.sprite = isSoundOff;
            setSound = false;
            PlayerPrefs.SetInt("Set_Sound", soundS);
        }
        else
        {
            soundS = 1;
            sound.sprite = isSoundOn;
            setSound = true;
            PlayerPrefs.SetInt("Set_Sound", soundS);
        }

    }
}
