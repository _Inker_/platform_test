﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;


public class Player : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float force;
    [SerializeField] private Rigidbody2D rigidBody;
    public float minimalHeight;
    public bool isCheatMode;
    public GroundDetection groundDetectin;
    private Vector3 direction;
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer spriteRenderer;   
    [SerializeField] private Arrow arrow;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float shootForce;
    [SerializeField] private float cooldown;
    [SerializeField] private int arrowsCount;
    [SerializeField] private Health health;
    [SerializeField] private BuffReciever buffReciever;
    public Health Health { get { return health; } }
    private Arrow currentArrow;
    private float bonusForce;
    private float bonusDamage;
    private float bonusHealth;
    private List<Arrow> arrowsPool;
    private bool isCooldown;
    private bool isJumping;
    private UICharacterController controller;

    #region Singleton

    public static Player Instance { get; set; }

    #endregion

    private void Awake()
    {
        Instance = this;
    }


    private void Start()
    {
        arrowsPool = new List<Arrow>();
        for (int i = 0; i < arrowsCount; i++)
        {
            var arrowTemp = Instantiate(arrow, arrowSpawnPoint);
            arrowsPool.Add(arrowTemp);
            arrowTemp.gameObject.SetActive(false);
        }

        buffReciever.OnBuffsChanged += BuffsBonus;
    }

    public void InitUIController(UICharacterController uiController)
    {
        controller = uiController;
        controller.Jump.onClick.AddListener(Jump);
        controller.Fire.onClick.AddListener(CheckShoot);
    }

    private void BuffsBonus()
    {
        var forceBuff = buffReciever.Buffs.Find(t => t.type == BuffType.Force);
        var damageBuff = buffReciever.Buffs.Find(t => t.type == BuffType.Damage);
        var armorBuff = buffReciever.Buffs.Find(t => t.type == BuffType.Armor);
        bonusForce = forceBuff == null ? 0 : forceBuff.additiveBonus;
        bonusHealth = armorBuff == null ? 0 : armorBuff.additiveBonus;
        health.SetHealth((int)bonusHealth);
        bonusDamage = damageBuff == null ? 0 : damageBuff.additiveBonus;
    }
    void FixedUpdate()
    {
        Move();
        animator.SetFloat("Speed", math.abs(direction.x));
        CheckFall();        
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
            Jump();
#endif      
    }

    private void Move()
    {
        animator.SetBool("isGrounded", groundDetectin.isGrounded);
        if (!isJumping && !groundDetectin.isGrounded)
            animator.SetTrigger("StartFall");

        isJumping = isJumping && !groundDetectin.isGrounded;
        direction = Vector3.zero; // (0,0)
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left;
        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right;
#endif
        if (controller.Left.IsPressed)
            direction = Vector3.left; // (-1,0)
        if (controller.Right.IsPressed)
            direction = Vector3.right; // (1,0)
        direction *= speed;
        direction.y = rigidBody.velocity.y;
        rigidBody.velocity = direction;


        if (direction.x > 0)
            spriteRenderer.flipX = false;
        if (direction.x < 0)
            spriteRenderer.flipX = true;
    }
    private void Jump()
    {
        if (groundDetectin.isGrounded)
        {
            rigidBody.AddForce(Vector2.up * (force + bonusForce), ForceMode2D.Impulse);
            animator.SetTrigger("StartJump");
            isJumping = true;
        }
    }
    void CheckShoot()
    {
            if (!isCooldown)
            {        
                animator.SetTrigger("Shoot");                            
            }
        
    }

    public void InitArrow()
    {
        currentArrow = GetArrowFromPool();
        currentArrow.SetImpulse(Vector2.right, 0, 0, this);
    }

    public void Shoot()
    {
        currentArrow.SetImpulse(Vector2.right, spriteRenderer.flipX ?
                    -force * shootForce : force * shootForce,(int)bonusDamage, this);

        StartCoroutine(Cooldown());
    }
    void CheckFall()
    {
        if (transform.position.y < minimalHeight && isCheatMode)
        {
            rigidBody.velocity = new Vector2(0, 0);
            transform.position = new Vector3(0, 0);
        }
        else if (transform.position.y < minimalHeight && !isCheatMode)
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator Cooldown()
    {
        isCooldown = true;
        yield return new WaitForSeconds(cooldown);
        isCooldown = false;
        yield break;
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowsPool.Count > 0)
        {
            var arrowTemp = arrowsPool[0];
            arrowsPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        return Instantiate(arrow, arrowSpawnPoint.position, Quaternion.identity);
    }

    public void ReturnArrowToPool(Arrow arrowTemp)
    {
        if (!arrowsPool.Contains(arrowTemp))
            arrowsPool.Add(arrowTemp);

        arrowTemp.transform.parent = arrowSpawnPoint;
        arrowTemp.transform.position = arrowSpawnPoint.transform.position;
        arrowTemp.gameObject.SetActive(false);
    }
 }

